package edu.ceis.ssiserviceceis.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration // le dice que esta clase es de configuracion
@EnableJpaAuditing //habilita la audtoria para jpa
public class PersistenceConfig {
}
