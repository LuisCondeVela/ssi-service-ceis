package edu.ceis.ssiserviceceis.domain;

import javax.persistence.*;

@Entity
public class SubCategory extends ModelBase {
    @Column
    private String name;
    @Column
    private String code;
    @OneToOne(optional = false)
    @JoinColumn(name = "category_id", foreignKey = @ForeignKey(name = "Fk_subcategory_category"))
    private Category category;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
